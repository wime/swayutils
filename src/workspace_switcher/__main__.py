import subprocess
from typing import Optional
import toml
import typer
from swayutils import osd, workspaces, shellutil


def main(direction: Optional[str] = None, ws_number: Optional[int] = None):
    path = "~/.config/swayutils/config.toml"
    config = toml.load(shellutil.read_path(path))
    ws_old = workspaces.get_current_workspace(icons=config["icons"])
    if direction:
        subprocess.run(["sway", "workspace", direction], capture_output=True)
    elif ws_number:
        subprocess.run(["sway", "workspace", "number", str(ws_number)], capture_output=True)
    else:
        return
    ws_new = workspaces.get_current_workspace(icons=config["icons"])
    direction = config.get("workspaces", {"direction": "vertical"}).get("direction", "vertical")
    arrows = config.get("osd").get("arrows")
    osd.workspace_osd(workspace1=ws_old, workspace2=ws_new, orientation="vertical", app_name="workspace_osd", arrows=arrows)


if __name__ == "__main__":
    typer.run(main)
