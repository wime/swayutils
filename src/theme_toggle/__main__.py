import toml
import typer
from swayutils import theme, shellutil


def main():
    config = toml.load(shellutil.read_path("~/.config/swayutils/config.toml"))
    modules = config["theme_switcher"]["modules"]
    theme_names = config["theme_switcher"].get("themes", {})
    theme.toggle_theme(included_modules=modules, theme_names=theme_names)


if __name__ == "__main__":
    typer.run(main)
