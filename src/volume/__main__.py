import subprocess
import toml
import typer
from swayutils import osd, shellutil


CMDS = {
    "raise": "amixer sset Master 1%+",
    "lower": "amixer sset Master 1%-",
    "toggle": "amixer set Master toggle"
}


def main(mode: str = "raise"):
    path = "~/.config/swayutils/config.toml"
    config = toml.load(shellutil.read_path(path))
    subprocess.run(CMDS[mode], shell=True, capture_output=True)
    osd.volume_osd(icons=config["icons"]["audio"], app="volume_osd")


if __name__ == "__main__":
    typer.run(main)
