import typer
from swayutils import processes


app = typer.Typer()
MODE_OPTIONS = {
    "toggle": processes.toggle_caffeine,
    "set": processes.set_caffeine,
    "kill": processes.kill_caffeine,
    "get": processes.get_caffeine
}


@app.command()
def main(mode: str = "toggle"):
    """
    Run commands related to caffeine.

    Parameters
        mode {str} -- Needs to be 'toggle', 'set', 'kill' or 'get'
    """
    try:
        print(MODE_OPTIONS[mode]())
    except KeyError:
        print(f"`mode` needs to be in {tuple(MODE_OPTIONS.keys())}")


if __name__ == "__main__":
    app()
