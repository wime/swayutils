from typing import Tuple
import toml
import typer
from swayutils import processes, shellutil


app = typer.Typer()
MODE_OPTIONS = {
    "toggle": processes.toggle_nightlight,
    "set": processes.set_nightlight,
    "kill": processes.kill_nightlight,
    "get": processes.get_nightlight
}


@app.command()
def main(mode: str = "toggle"):
    """
    Run commands related to caffeine.

    Parameters
        mode {str} -- Needs to be 'toggle', 'set', 'kill' or 'get'
    """
    config = toml.load(shellutil.read_path("~/.config/swayutils/config.toml"))
    config = config["nightlight"]
    try:
        print(MODE_OPTIONS[mode](config=config))
    except TypeError:
        print(MODE_OPTIONS[mode]())
    except KeyError:
        print(f"`mode` needs to be in {tuple(MODE_OPTIONS.keys())}")


if __name__ == "__main__":
    app()
