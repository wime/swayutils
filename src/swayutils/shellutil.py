import subprocess
import json
import os
from typing import Iterable


def read_path(path: str) -> str:
    """
    Translate path to Python readable string.

    Parameters
        path {str} -- Path to translate.
    Returns
        {str} -- Translated path.
    """
    return path.replace("~", os.environ["HOME"])


def get_shell_output(cmd: str) -> str:
    """
    Get output of shell command.

    Parameters
        cmd {str} -- Shell command to execute.
    Returns
        {str} Output from shell command.
    """
    result = subprocess.run(cmd, shell=True, capture_output=True)
    return result.stdout.decode("utf-8")


def get_open_notifications() -> Iterable:
    """
    Get app names of active notifications.

    Returns
        {Iterable} -- List of app names of active notifications.
    """
    notifs = get_shell_output("makoctl list")
    notifs = json.loads(notifs)
    apps = [notif["app-name"]["data"] for notif in notifs["data"][0]]
    return apps


def check_app_is_running(app: str) -> bool:
    """
    Check if application is running.

    Parameters
        app {str} -- Application name.
    Returns
        {bool} -- True if running, False if not.
    """
    pid = get_shell_output(f"pidof {app}").replace("\n", "")
    return bool(pid)


def run_app(
    app: str,
    force_restart: bool = False,
    notify: bool = False,
    notification_icon: str = ""
) -> None:
    """Start application"""
    if force_restart:
        subprocess.run(["killall", app])
    subprocess.run(f"swaymsg exec '{app}'", shell=True)
    if notify:
        subprocess.run(
            ["notify-send", f"{app} activated.", "-i", notification_icon]
        )
