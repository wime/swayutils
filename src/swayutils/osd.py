import subprocess
from typing import Dict, Literal, Optional, Iterable

from . import workspaces, processes, datamodel


DEFAULT_ARROWS = {"up": "↑", "down": "↓", "left": "←", "right": "→"}

DIRECTION = Literal["next", "prev"]
ORIENTATION = Literal["horizontal", "vertical"]


def show_osd(message: str, app: str, params: Optional[Iterable] = None) -> None:
    """Show osd with given message."""
    if not params:
        params = []
    message = [
        "notify-send",
        message,
        "-a",
        app,
        "-h",
        "string:x-canonical-private-synchronous:sys-notify",
    ] + params
    subprocess.run(message)


def _get_arrow(
    direction: DIRECTION, orientation: ORIENTATION, arrows: Dict = DEFAULT_ARROWS
) -> str:
    """Get arrow based on orientation and direction."""
    next = {"horizontal": "right", "vertical": "down"}[orientation]
    prev = {"horizontal": "left", "vertical": "up"}[orientation]
    return arrows[{"next": next, "prev": prev}[direction]]


def flow_osd(
    message1: str,
    message2: str,
    orientation: ORIENTATION,
    direction: DIRECTION,
    app: str,
    arrows: Dict = DEFAULT_ARROWS,
) -> None:
    """Show OSD for a flow between message 1 and message 2."""
    arrow = _get_arrow(direction=direction, orientation=orientation, arrows=arrows)
    merge_method = {"horizontal": " ", "vertical": "\n"}[orientation]
    message = merge_method.join([message1, arrow, message2])
    show_osd(message=message, app="workspace_osd")


def _get_workspace_switch_direction(
    workspace1: datamodel.Workspace, workspace2: datamodel.Workspace
) -> Iterable:
    """Check if it is possible to say wich workspace is before the other."""
    try:
        ws1_number = int(workspace1.name)
        ws2_number = int(workspace2.name)
    except ValueError:
        ws1 = workspace1
        ws2 = workspace2
    else:
        if ws1_number <= ws2_number:
            ws1 = workspace1
            ws2 = workspace2
            direction = "next"
        else:
            ws1 = workspace2
            ws2 = workspace1
            direction = "prev"
    return ws1, ws2, direction


def workspace_osd(
    workspace1: datamodel.Workspace,
    workspace2: datamodel.Workspace,
    orientation: ORIENTATION,
    app_name: str = "workspace_osd",
    arrows: Dict = DEFAULT_ARROWS,
) -> None:
    """Show Workspace OSD"""
    ws1, ws2, direction = _get_workspace_switch_direction(
        workspace1=workspace1, workspace2=workspace2
    )
    ws1 = f"{ws1.icon}{ws1.name}"
    ws2 = f"{ws2.icon}{ws2.name}"
    flow_osd(
        message1=ws1,
        message2=ws2,
        orientation=orientation,
        direction=direction,
        app="workspace_switcher",
        arrows=arrows,
    )


def volume_osd(icons: Dict, app: str = "volume_osd"):
    audio = processes.get_audio(icons=icons)
    show_osd(message=f"{audio.__str__()}", app=app)
