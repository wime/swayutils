import subprocess
from typing import Iterable, Dict

from . import shellutil, theme_modules, datamodel


THEME_MODULES = {
    "fuzzel": theme_modules.set_fuzzel_theme,
    "mako": theme_modules.set_mako_theme,
    "sway": theme_modules.set_sway_theme,
    "kitty": theme_modules.set_kitty_theme,
    "waybar": theme_modules.set_waybar_theme,
    "gtk4": theme_modules.set_gtk4_theme,
    "gtk3": theme_modules.set_gtk3_theme,
    "wezterm": theme_modules.set_wezterm_theme,
}


def _get_gtk4_theme() -> str:
    """Find current color scheme preferance"""
    theme = shellutil.get_shell_output(
        "gsettings get org.gnome.desktop.interface color-scheme"
    )
    return theme.split("\n")[0].replace("'", "").split("-")[1]


def _theme_to_state(theme, icons=None):
    value = {"light": 1, "dark": 0}[theme]
    return datamodel.SystemProcess(
        state=theme,
        quantity=datamodel.Quantity(value=value, unit="state"),
        icons=icons
    )


def _set_module_theme(module: str, theme: datamodel.Theme, theme_names: Dict):
    module_theme_names = theme_names.get(module, None)
    if module_theme_names:
        module_theme = datamodel.Theme(
            state=theme.state,
            light_theme=theme_names[module]["light"],
            dark_theme=theme_names[module]["dark"],
        )
    else:
        module_theme = theme
    THEME_MODULES[module](theme=module_theme)


def set_theme(
    theme: datamodel.SystemProcess,
    included_modules: Iterable[str],
    theme_names: Dict = {},
):
    for module in included_modules:
        if module == "mako":
            pass
        else:
            try:
                _set_module_theme(module=module, theme=theme, theme_names=theme_names)
            except Exception:
                pass
    subprocess.run(["sway", "reload"], capture_output=True)
    if "mako" in included_modules:
        try:
            _set_module_theme(module="mako", theme=theme, theme_names=theme_names)
        except Exception:
            pass


def get_theme(icons=None):
    current_theme = _get_gtk4_theme()
    return _theme_to_state(current_theme, icons=icons)


def get_theme_from_toggle():
    themes = ["light", "dark"]
    theme = {themes[0]: themes[1], themes[1]: themes[0]}[get_theme().state]
    return _theme_to_state(theme)


def toggle_theme(included_modules: Dict, theme_names: Dict):
    theme = get_theme_from_toggle()
    set_theme(theme=theme, included_modules=included_modules, theme_names=theme_names)
