import subprocess
from dataclasses import dataclass, field, InitVar
import json
from typing import Iterable, Optional, Dict
from . import shellutil, datamodel


def _check_node(node, apps, icons: Optional[Dict] = None):
    try:
        app_id = node["app_id"]
    except KeyError:
        _iterate_nodes(node, apps)
    else:
        if not app_id:
            try:
                app_id = node["window_properties"]["class"]
            except KeyError:
                pass
        apps.append(
            datamodel.App(
                name=app_id,
                pid=node["pid"],
                focused=node["focused"],
                content=node["name"],
                icons=icons,
            )
        )


def _iterate_nodes(node, apps, icons: Optional[Dict] = None):
    for subnode in node["nodes"]:
        _check_node(subnode, apps, icons=icons)
    for floating_node in node["floating_nodes"]:
        _check_node(floating_node, apps, icons=icons)


def get_workspaces(icons: Optional[Dict] = None) -> Iterable[datamodel.Workspace]:
    """
    Get all opened workspaces.

    Returns
        {Iterable[Workspace]} -- List with workspaces and active applications.
    """
    result = json.loads(shellutil.get_shell_output("swaymsg -t get_tree"))
    workspaces = []
    for screen in result["nodes"]:
        for ws in screen["nodes"]:
            apps = []
            _iterate_nodes(ws, apps, icons=icons["apps"])
            workspace = datamodel.Workspace(
                name=ws["name"], apps=apps, icons=icons["workspaces"]
            )
            workspaces.append(workspace)
    return workspaces


def workspace_overview(
    workspaces: Optional[Iterable[datamodel.Workspace]] = None,
    v_spaces: int = 0,
    h_spaces: int = 1,
):
    if not workspaces:
        workspaces = get_workspaces()
    ws_strings = [ws.__str__(spaces=h_spaces) for ws in workspaces]
    return ("\n" * (v_spaces + 1)).join(ws_strings)


def get_workspace_height():
    result = json.loads(shellutil.get_shell_output("swaymsg -t get_tree"))
    return result["nodes"][0]["rect"]["height"]


def get_current_workspace(icons: Optional[Dict] = None):
    workspaces = get_workspaces(icons=icons)
    return {ws.focused: ws for ws in workspaces}[True]


def get_current_app():
    current_ws = get_current_workspace()
    return {app.focused: app for app in current_ws.apps}.get(True, None)
