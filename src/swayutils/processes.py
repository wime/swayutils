import subprocess
import json
import datetime as dt
from typing import Dict, Optional

from . import shellutil, datamodel


TEMPFILE_NIGHTLIGHT = "~/.temp/nightlight.json"


def get_audio(icons: Optional[Dict] = None) -> datamodel.SystemProcess:
    result = shellutil.get_shell_output("pactl list sinks")
    for item in result.split("\n"):
        try:
            key, value = item.strip("\t").replace("\n", "").split(": ", 1)
        except ValueError:
            pass
        else:
            if key == "Volume":
                percent = float(value.split("/")[1][1:-2].replace(",", "."))
                percent = int(round(percent))
            elif key == "Mute":
                state = {"yes": "muted", "no": "volume"}[value]
            elif key == "Description":
                description = value
    if state == "muted":
        percent = 0
    return datamodel.SystemProcess(
        state=state,
        quantity=datamodel.Quantity(value=percent, unit="%"),
        text=description,
        icons=icons
    )


def get_battery(icons: Optional[Dict] = None) -> datamodel.SystemProcess:
    result = shellutil.get_shell_output("upower -i /org/freedesktop/UPower/devices/battery_BAT0")
    battery_dict = {}
    for values in result.split("\n"):
        dict_input = values.replace(" ", "").replace("\n", "").split(":")
        if len(dict_input) == 2:
            battery_dict[dict_input[0]] = dict_input[1]
    state = battery_dict["state"]
    percent = int(round(float(battery_dict["percentage"][:-1].replace(",", "."))))
    return datamodel.SystemProcess(
        state=state,
        quantity=datamodel.Quantity(value=percent, unit="%"),
    )


def get_brightness(icon: Optional[str] = None) -> datamodel.SystemProcess:
    value = int(round(float(shellutil.get_shell_output("light"))))
    return datamodel.SystemProcess(
        state="ON",
        quantity=datamodel.Quantity(value=value, unit="%"),
        icons=icon
    )


def get_caffeine(icon: Optional[str] = None) -> datamodel.SystemProcess:
    state = not shellutil.check_app_is_running("swayidle")
    return datamodel.SystemProcess(
        state={True: "ON", False: "OFF"}[state],
        quantity=datamodel.Quantity(value=int(state), unit="state"),
        icons=icon
    )


def set_caffeine():
    subprocess.run(["pkill", "swayidle"])
    return "Caffeine is set"


def kill_caffeine():
    subprocess.run("swayidle & disown", shell=True)
    return "Caffeine is killed"


def toggle_caffeine():
    if not shellutil.check_app_is_running("swayidle"):
        return kill_caffeine()
    else:
        return set_caffeine()


def get_media(icon: Optional[str] = None) -> datamodel.SystemProcess:
    """Get media output"""
    title = shellutil.get_shell_output("playerctl metadata -f '{{title}}'").replace(
        "\n", ""
    )
    artist = shellutil.get_shell_output("playerctl metadata -f '{{artist}}'").replace(
        "\n", ""
    )
    status = shellutil.get_shell_output("playerctl status").lower().replace("\n", "")
    title = title.split("(")[0]
    title = title[:30]
    artist = artist[:30]
    if len(title) > 0:
        value = 1
    else:
        value = 0
    return datamodel.SystemProcess(
        state=status,
        quantity=datamodel.Quantity(value=value, unit="state"),
        info={"artist": artist, "title": title},
        icons=icon
    )


def get_wifi(icons: Optional[Dict] = None) -> datamodel.SystemProcess:
    """Get wifi connection."""
    connection = shellutil.get_shell_output("nmcli con show --active")
    if connection == "\n":
        return datamodel.SystemProcess(
            state="diconnected",
            quantity=datamodel.Quantity(value=0, unit="state"),
            text="NOT CONNECTED",
            icons=icons
        )
    return datamodel.SystemProcess(
        state="wifi",
        quantity=datamodel.Quantity(value=1, unit="state"),
        text=connection.split("\n")[1].split(" ")[0],
        icons=icons
    )


def get_nightlight():
    state = shellutil.check_app_is_running("wlsunset")
    if state:
        try:
            with open(shellutil.read_path(TEMPFILE_NIGHTLIGHT), "r") as f:
                last_executed = json.load(f)
            sunrise = last_executed["sunrise"]
            sunset = last_executed["sunset"]
        except Exception:
            sunrise = "06:00"
            sunset = "19:00"
        now = dt.datetime.now()
        sunrise = dt.datetime.strptime(now.strftime("%Y%m%dT"+sunrise), "%Y%m%dT%H:%M")
        sunset = dt.datetime.strptime(now.strftime("%Y%m%dT"+sunset), "%Y%m%dT%H:%M")
        if now > sunrise and now < sunset:
            state = False
    return datamodel.SystemProcess(
        state={True: "ON", False: "OFF"}[state],
        quantity=datamodel.Quantity(value=int(state), unit="state"),
    )


def _set_nightlight(sunrise: str, sunset: str, temp_day: int, temp_night: int):
    kill_nightlight()
    subprocess.run(
        f"wlsunset -S {sunrise} -s {sunset} -T {temp_day} -t {temp_night} & disown",
        shell=True,
    )
    subprocess.run(["touch", shellutil.read_path(TEMPFILE_NIGHTLIGHT)])
    with open(shellutil.read_path(TEMPFILE_NIGHTLIGHT), "w") as f:
        json.dump({"sunrise": sunrise, "sunset": sunset}, f)


def set_nightlight(config: Dict):
    config_sunrise = config.get("sunrise", "06:00")
    config_sunset = config.get("sunset", "06:00")
    temp_day = config.get("temp_day", 6500)
    temp_night = config.get("temp_night", 3600)
    _set_nightlight(sunrise=config_sunrise, sunset=config_sunset, temp_day=temp_day, temp_night=temp_night)


def kill_nightlight():
    subprocess.run(["killall", "wlsunset"], capture_output=True)


def toggle_nightlight(config: Dict):
    if get_nightlight().state == "ON":
        activity = "kill"
    else:
        activity = "set"
    config_sunrise = config.get("sunrise", "06:00")
    config_sunset = config.get("sunset", "06:00")
    now = dt.datetime.now()
    sunrise = dt.datetime.strptime(now.strftime("%Y%m%dT"+config_sunrise), "%Y%m%dT%H:%M")
    sunset = dt.datetime.strptime(now.strftime("%Y%m%dT"+config_sunset), "%Y%m%dT%H:%M")
    temp_day = config.get("temp_day", 6500)
    temp_night = config.get("temp_night", 3600)
    if now > sunrise and now < sunset:
        if activity == "kill":
            _set_nightlight(sunrise=config_sunrise, sunset=config_sunset, temp_day=temp_day, temp_night=temp_night)
        else:
            _set_nightlight(sunrise="23:58", sunset="23:59", temp_day=temp_day, temp_night=temp_night)
    elif activity == "kill":
        kill_nightlight()
    else:
        _set_nightlight(sunrise=config_sunrise, sunset=config_sunset, temp_day=temp_day, temp_night=temp_night)


def get_notifications():
    notifications = shellutil.get_shell_output("makoctl list")
    notifications = json.loads(notifications)["data"][0]
    notifs = []
    for notification in notifications:
        notifs.append(datamodel.Notification(
            app_name=notification["app-name"]["data"],
            id=notification["id"]["data"]
        ))
    return notifs


def notification_is_active(app_name: str) -> bool:
    return app_name in [notif.app_name for notif in get_notifications()]


def dismiss_notification(app_name: str):
    try:
        id = {notif.app_name: notif.id for notif in get_notifications()}[app_name]
        subprocess.run(["makoctl", "dismiss", "-n", str(id)])
    except KeyError:
        return

