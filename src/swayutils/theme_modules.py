import subprocess
import os
import shutil
import configparser

from . import shellutil, datamodel


def set_fuzzel_theme(theme: datamodel.Theme) -> None:
    """Set fuzzel theme."""
    theme_path = f"{os.environ['HOME']}/.config/fuzzel/themes/{theme.state}.ini"
    config_path = f"{os.environ['HOME']}/.config/fuzzel/fuzzel.ini"
    colors = configparser.ConfigParser()
    colors.read(theme_path)
    config = configparser.ConfigParser()
    config.read(config_path)
    try:
        config["colors"] = colors["colors"]
    except TypeError:
        raise TypeError(f"No `colors` section in {theme_path}")
    with open(config_path, "w") as f:
        config.write(f)


def set_gtk4_theme(theme: datamodel.Theme) -> None:
    subprocess.run(
        f"gsettings set org.gnome.desktop.interface color-scheme prefer-{theme.state}",
        shell=True,
    )


def set_gtk3_theme(theme: datamodel.Theme) -> None:
    theme_name = theme.__name__()
    subprocess.run(
        f"gsettings set org.gnome.desktop.interface gtk-theme {theme_name}", shell=True
    )


def set_kitty_theme(theme: datamodel.Theme) -> None:
    theme_name = theme.__name__()
    subprocess.run(f"kitty +kitten themes --reload-in=all {theme_name}", shell=True)


def set_wezterm_theme(theme: datamodel.Theme) -> None:
    theme_name = theme.__name__()
    subprocess.run(f"sed -i '/color_scheme = /c\\    color_scheme = \"{theme_name}\",' $HOME/.wezterm.lua", shell=True)


def set_sway_theme(theme: datamodel.Theme) -> None:
    """Set theme of sway"""
    source = f"~/.config/sway/themes/theme_{theme.state}"
    destination = "~/.config/sway/config.d/theme"
    shutil.copy(shellutil.read_path(source), shellutil.read_path(destination))


def set_mako_theme(theme: datamodel.Theme) -> None:
    subprocess.run(f"makoctl mode -s {theme.state}", shell=True, capture_output=True)


def set_waybar_theme(theme: datamodel.Theme) -> None:
    source = f"~/.config/waybar/themes/{theme.state}.css"
    destination = "~/.config/waybar/theme.css"
    shutil.copy(shellutil.read_path(source), shellutil.read_path(destination))
