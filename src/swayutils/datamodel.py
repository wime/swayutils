from dataclasses import dataclass, field, InitVar
from numbers import Number
from typing import Literal, Dict, Optional, Iterable


UNITS = Literal["state", "%"]
THEMES = Literal["light", "dark"]

DEFAULT_QUANTITY_ICONS = {
    "percent": {"filled": "", "open": ""},
    "state": {1: "", 0: ""},
}


def _get_percent_bar(
    value: Number, icons: Dict = DEFAULT_QUANTITY_ICONS["percent"]
) -> str:
    """
    Create percent bar.

    Parameters
        value {Number} -- Percent.
    Returns
     {str} -- Percent bar.
    """
    filled = int(round(value, -1) * 1e-1)
    open = 10 - filled
    return filled * (icons["filled"] + " ") + open * (icons["open"] + " ")


@dataclass
class Quantity:
    value: Number
    unit: UNITS

    def __bar__(self, icons: Dict = DEFAULT_QUANTITY_ICONS):
        if self.unit == "%":
            return _get_percent_bar(self.value)
        elif self.unit == "state":
            return icons["state"][self.value]

    def __str__(self):
        if self.unit == "%":
            return f"{self.value}{self.unit}"
        elif self.unit == "state":
            return {1: "ON", 0: "OFF"}[self.value]


@dataclass
class SystemProcess:
    state: str
    quantity: Quantity
    text: Optional[str] = None
    info: Optional[Dict] = None
    icon: Optional[str] = None
    icons: InitVar[Optional[Dict | str]] = None

    def __post_init__(self, icons: Optional[Dict]):
        if icons:
            try:
                self.icon = icons.get(self.state, None)
            except AttributeError:
                self.icon = icons

    def __str__(
        self, showbar: bool = True, showquantity: bool = True, oneliner: bool = False
    ):
        if self.icon:
            icon = f"{self.icon:^3}"
        else:
            icon = ""
        if showquantity:
            quantity = f"{self.quantity.__str__():>4}"
        else:
            quantity = ""
        if not showbar:
            return f"{icon}{quantity}"
        if oneliner:
            return f"{icon} {self.quantity.__bar__()}{quantity}"
        else:
            return f"{icon}{quantity}\n{self.quantity.__bar__()}"


@dataclass
class Theme:
    state: THEMES
    light_theme: str = "light"
    dark_theme: str = "dark"

    def __name__(self):
        return {"light": self.light_theme, "dark": self.dark_theme}[self.state]


@dataclass
class App:
    """Descripton of open application."""

    name: str
    pid: int
    focused: bool = False
    content: str = ""
    icon: Optional[str] = None
    icons: InitVar[Optional[Dict]] = None

    def _fix_name(self):
        self.name = self.name.split(".")[-1]
        self.name = self.name.split("-default")[0]

    def _set_icon(self, icons: Optional[Dict]):
        if not icons:
            self.icon = None
            return
        try:
            self.icon = icons.get(self.name.lower(), icons["default"])
        except KeyError:
            self.icon = None

    def __post_init__(self, icons: Optional[str] = None):
        self._set_icon(icons=icons)
        self._fix_name()


@dataclass
class Workspace:
    """Description of a workspace."""

    name: str
    apps: Iterable[App]
    focused: bool = field(init=False)
    icon: Optional[str] = None
    icons: InitVar[Optional[Dict]] = None

    def _set_focused(self):
        try:
            self.focused = max([app.focused for app in self.apps])
        except ValueError:
            if len(self.apps) == 0 and self.name != "__i3_scratch":
                self.focused = True
            else:
                self.focused = False

    def _set_icon(self, icons: Optional[Dict] = None):
        if not icons:
            return
        try:
            self.icon = icons.get(self.name, icons["default"])
        except KeyError:
            return

    def __post_init__(self, icons: Optional[Dict] = None):
        self._set_icon(icons=icons)
        self._set_focused()

    def __str__(self, spaces: int = 1):
        apps = (" " * spaces).join([app.icon for app in self.apps])
        focused = {True: "*", False: ""}
        ws = f"{focused[self.focused]}{self.icon}{self.name}"
        return f"{ws:>5}:{' '*(spaces+1)}{apps}"


@dataclass
class Notification:
    app_name: str
    id: int


